#!/usr/bin/env bash

get_timestamp() {
  date +"%Y-%m-%d_%H-%M-%S"
}

timestamp=$1
if [ -z "$1" ]
then
  echo "./gobackupdb <timestamp|20200720>"
  echo "timestamp is empty"
  timestamp=$(date +"%Y-%m-%d_%H-%M-%S")
fi

echo $timestamp

